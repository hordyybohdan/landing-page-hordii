<?php

class Order{

  private $data;
  private $str;
  private $file_name;

  function __construct(){
    $this->data=json_decode( file_get_contents('php://input'), true );
    $this->str = sprintf("%s %s", $this->data['name'], $this->data['email']."\r\n");
    $this->file_name = "./src/database/data" . date("H:i:s") . ".txt";
  }
  function getStr(){
    return $this->str;
  }

  function getFileName(){
    return $this->file_name;
  }

}

/*
class Order
{
  private $name;
  private $email;

  public function __construct(string $name, string $email)
  {
    $this->name = $name;
    $this->email = $email;
  }

  public function getName()
  {
    return $this->name;
  }

  public function getEmail()
  {
    return $this->email;
  }

  public static function deserialize(array $data)
  {
    return new self($data['name'], $data['email']);
  }

  public function serialize() : array
  {
    return [
      'name' => $this->name,
      'email' => $this->email
    ];
  }

  public function toJsonString() : string
  {
    return json_encode($this->serialize());
  }

  public static function fromJsonString(string $str)
  {
    $data = json_decode($str, true);
    return new self($data['name'], $data['email']);
  }

  public function toCsvString()
  {
    return $this->name.','.$this->email.'\n';
  }
}
*/


?>
