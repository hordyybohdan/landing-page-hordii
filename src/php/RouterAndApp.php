<?php

class App
{
private $fileOrderManager;
private $pageManager;
private $order;
private $routes = [
'/' => 'getIndex',
'/admin' => 'getAdminPage',
'/saveorder' => 'save',
'/list' => 'list'
];
function __construct($fileOrderManager,$pageManager,$order)
{
  $this->fileOrderManager=$fileOrderManager;
  $this->pageManager= $pageManager;
  $this->order=$order;

}
public function run()
{
  $path = parse_url($_SERVER['REQUEST_URI']);//витягуємо шлях
  $method = $this->routes[$path['path']] ?? 'getIndex';//шукаємо співпадіння і записуємо в метод
  if(!method_exists($this, $method)) { //перевіряємо  наявність методу
    return; // throw exception
};
  $this->$method();
}
public function getIndex()
{
  return $this->pageManager->getIndex();
}
public function save()
{
  $this->fileOrderManager->save($this->order);
}
public function getAdminPage() {
  $this->pageManager->getAdminPage();
}
public function list() {
  $this->fileOrderManager->list();
}

}

?>
